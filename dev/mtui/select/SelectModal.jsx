'use strict';

import './select.scss';
import "babel-polyfill"
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import assign from '../utils/assign';

class SelectModal extends Component {

	//构造函数
	constructor (props) {
		super(props);
		this.state = {
			cName : ''
		}
		this.num = 0;
	}

	//选择option
	clickOption(elem){
		this.props.showOrHide(true,elem.props,null)
	}

	//设置值
	componentDidUpdate(prevProps, prevState) {
	 	if(prevProps.show != this.props.show){
	 		let cName = ' mt-select-animate'
	 		let self = this;
	 		setTimeout(function(){
	 			self.setState({
		 			cName : self.props.show ? cName:''
		 		})
	 		},10)
	 	}
	}

	render(){
		if(this.props.set){
			var {height, left, top, width} = this.props.set;
			var style = assign([{
				display: this.props.show?'block':'none'
			},{
				left : left,
				top : top + height
			},this.props.modalStyle])
		}
		var self = this;
		var cName = ["mt-select",this.state.cName]; //mt-select-ie10

		if(!MT_IE9){
			cName.push('mt-select-ie10')
		}

		return <div className={cName.join(' ')} style={style} id={this.props.mid}>
					{
						this.props.children.map(function(elem, index) {
							return <div onClick={self.clickOption.bind(self,elem)} key={index} className="mt-select-option">{elem.props.children}</div>;
						})
					}
			   </div>
	}
}

//主页
export default SelectModal;