'use strict';

import './select.scss';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import SelectModal from './SelectModal';
import {closest} from '../utils/select';
import {offsetTop,offsetLeft} from '../utils/offset';

class Select extends Component {

	//默认参数
	static defaultProps = {
		mid : "selectId", //默认box 的 ID
		modalStyle:{width:90}, //modal的样式
		visible : false, //框默认隐藏
		showBack : null, //显示时候的回调
		onChange : null, //关闭时候的回调
		trigger : "hover", //触发下拉的行为
		placeholder: '下拉选择框',
		defaultValue:undefined, //不受控组件
		value:undefined //受控组件
	}
	
	//构造函数
	constructor (props) {
		super(props);
		this.state = {
			show: props.visible,
			set: {
				left:null,
				top:null,
				width:null,
				height:null
			},
			text:'',
			value: ''
		}
		this.div = document.createElement('div');
		this.div.setAttribute('class','mt-div');
	}

	//渲染div
	renderDiv(){
		var _this = this;
		var dom = document.getElementById(this.props.mid);
		//首次渲染即可
		if(!dom){
			this.setState({
				set:{
					left: offsetLeft(this.refs.btn),
					top: offsetTop(this.refs.btn),
					width: this.refs.btn.offsetWidth,
					height: this.refs.btn.offsetHeight
				}
			})
 			document.body.appendChild(this.div);
 			this.renderDOM();
		}
	}

	renderDOM(){
		ReactDOM.render(<SelectModal {...this.state} showOrHide={this.showOrHide.bind(this)} show={this.state.show} set={this.state.set} {...this.props}/>,this.div);
	}

	//更新弹窗里面的数据
	componentDidUpdate(prevProps){

		//受控组件
		if(this.props.value || this.props.defaultValue){
			//console.log('受控组件',this.props.value,prevProps.value);
			this.renderDOM();
		}
	}

	//受控组件方法
	componentWillUpdate(nextProps, nextState) {
		if(this.props.value && this.props.value != nextProps.value){
			let self = this;
			nextProps.children.map(function(elem, index) {
				if(elem.props.value == nextProps.value){
					self.setValue(elem.props)
				}
			})
		}
	}

	//设置value
	setValue(data,callback){
		this.setState({
			text: data.children,
			value: data.value
		},function(){
			if(callback){
				callback(data)
			}
		})
	}

	//隐藏显示
	showOrHide(show,data,callback,value){

		//console.log(show,data)
		this.hoverHandler = null;
		var self = this;
		this.renderDiv();
		this.setState({
			show: !show,
			set:{
				left: offsetLeft(this.refs.btn),
				top: offsetTop(this.refs.btn),
				width: this.refs.btn.offsetWidth,
				height: this.refs.btn.offsetHeight
			}
		},function(){
			if(!show){
				//显示弹窗
				if(self.props.showBack){
					self.props.showBack()
				}
			}else{
				//选择值
				if(self.props.onChange && data){
					if(this.props.value){
						self.props.onChange(data)
						return
					}else{
						this.setValue(data, self.props.onChange(data))	
					}		
				}
			}
			if(callback){
				callback();
			}
		})
	}

	//点击其他区域，隐藏下拉框
	triggerBlank(events){
		let _this = this;

		var callback = function(){
			document.removeEventListener(events,handler)
		}
		var handler = function(e){
			const self = this;
			if(!closest(e.target,'.mt-select') && !closest(e.target,'.mt-select-input')){
				_this.showOrHide(true,null,callback)
			}
		}
		document.addEventListener(events,handler)
	}

	componentWillUnmount() {
	    this.refs.btn.removeEventListener('mouseenter',this.hoverHandler);
	    this.div.remove();
        ReactDOM.unmountComponentAtNode(this.div);
	}

	//初始化状态
	componentDidMount() {

		let self = this;
		//如果默认是显示，直接显示DIV
		if(this.props.visible){
			self.renderDiv();
		}

		//设置默认值
		if(this.props.defaultValue || this.props.value){
			this.props.children.map(function(elem, index) {
				if(elem.props.value == self.props.defaultValue || elem.props.value == self.props.value){
					self.setState({
						text: elem.props.children,
						value: elem.props.value
					})
				}
			})
		}

		//hover事件
		if(this.props.trigger == 'hover'){
			const _this = this;

			this.hoverHandler = function(e){
				_this.showOrHide(false,null,function(){
					_this.triggerBlank('mousemove')
				})
			}
			this.refs.btn.addEventListener('mouseenter',this.hoverHandler);
		}
	}

	//点击事件
	handleClick(){
		const _this = this;
		if(this.props.trigger == 'click'){
			this.showOrHide(this.state.show,null,function(){
				_this.triggerBlank('click')
			});
		}else{
			return
		}
	}

	render(){
		return <div style={{width:this.props.modalStyle.width||120}} ref="btn" className="mt-select-input" onClick={this.handleClick.bind(this)}>
					<span>{this.state.text||<i className="mt-placeholder">{this.props.placeholder}</i>}</span> 
					<i className="iconfont icon-arrowd"></i>
				</div>
	}
}

//选项卡
class Option extends Component {
	render(){
		return <span>{this.props.children}</span>
	}
}

Select.Option = Option;

//主页
export default Select;