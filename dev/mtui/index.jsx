/**
* @type MTUI2.0
* @author : Mantou
*/
import './public.scss';

import Grid from './grid/Grid'
import Button from './button/Button'
import Input from './input/Input'
import PageList from './pagelist/PageList'
import Modal from './modal/Modal'
import Dropdown from './dropDown/Dropdown'
import Select from './select/Select'
import BackTop from './backtop/BackTop'
import Tabs from './Tabs/Tabs'
import DatePicker from './DatePicker/DatePicker'
import Switch from './switch/Switch'
import Radio from './radio/Radio'
import Checkbox from './checkbox/Checkbox'
import Panle from './panle/Panle'
import Slider from './slider/Slider'
import SliderBar from './sliderBar/SliderBar'
import Tree from './tree/Tree'
import Validate from './validate/Validate'
import Swiper from './swiper/Swiper'
import Collapse from './collapse/Collapse'

//工具
import Tip from './Tip/Tip'

//配置信息
export {
	Grid,
	Button,
	Input,
	Tip,
	PageList,
	Modal,
	Dropdown,
	Select,
	BackTop,
	Tabs,
	DatePicker,
	Switch,
	Radio,
	Checkbox,
	Panle,
	Slider,
	SliderBar,
	Tree,
	Validate,
	Swiper,
	Collapse
}

window.MT_IE9 = false; //如果是IE9 就是true
if(navigator.appName == "Microsoft Internet Explorer"){
	window.MT_MS = 'IE';
	let ms = navigator.appVersion.split(";")[1].replace(/[ ]/g,"");
	if(~~ms.replace('MSIE','') > 9){
		window.MT_IE9 = false;
	}else{
		window.MT_IE9 = true;
	}
}else{
	window.MT_MS = 'other';
}