'use strict';

import './modal.scss';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import assign from '../utils/assign';

class ModalBox extends Component {

	//构造函数
	constructor (props) {
		super(props);
	}

	closeModal(){
		this.props.showOrHide(true)
	}

	render(){
		if(this.props.set){
			var {height, left, top, width} = this.props.set;
			var style = assign([{
				left : left,
				top : top + height
			},this.props.modalStyle])
		}
		
		return <div className="mt-modal" id={this.props.mid} style={{display: this.props.show?'block':'none'}}>
					<div className={'mt-modal-box '+this.props.modalClassName} style={style}>
			        	<a className="mt-modal-close" onClick={this.closeModal.bind(this)}><i className="iconfont icon-close"></i></a>
			        	{this.props.children}
			        </div>
			   </div>
	}
}

//主页
export default ModalBox;