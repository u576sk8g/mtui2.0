'use strict';

import './input.scss';
import React, { Component, PropTypes } from 'react';
import {offsetLeft,offsetTop} from '../utils/offset';

class Input extends Component {
	static defaultProps = {
		size: 'nm', //lg , nm, sm, xs 大 ，正常，小，超小
		type: 'text',
		prefix : null, //前面的图标
		suffix : null, //后面的图标
		onPressEnter : null, //回车的回调
		validateInfo: null //表单验证提示
	}

	//构造函数
	constructor (props) {
		super(props);
	}

	render(){
		const {size, prefix, suffix, onPressEnter, className,validateInfo, ...other} = this.props;

		let cName = (className?className+' ':'') + 'mt-input-'+(!size?'nm':size) + ' mt-input';
		return (
	        <span className={cName}>
	        	{prefix ? <span className="mt-input-prefix">{prefix}</span> : null}
	        	<input {...other}/>
	        	{suffix ? <span className="mt-input-suffix">{suffix}</span> : null}
	        	{validateInfo}
	        </span>
	    );
	}
}

//主页
export default Input;