'use strict';

import './dropDown.scss';
import "babel-polyfill"
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import assign from '../utils/assign';

class DropModal extends Component {

	//构造函数
	constructor (props) {
		super(props);
	}

	render(){
		if(this.props.set){
			var {height, left, top, width} = this.props.set;
			var style = assign([{
				display: this.props.show?'block':'none'
			},{
				left : left,
				top : top + height
			},this.props.modalStyle])
		}
		
		return <div className="mt-dropdown" style={style} id={this.props.mid}>
					{this.props.children}
			   </div>
	}
}

//主页
export default DropModal;