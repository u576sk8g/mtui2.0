'use strict';

import './dropDown.scss';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import DropModal from './DropModal';
import {closest} from '../utils/select';
import {offsetTop,offsetLeft} from '../utils/offset';

class DropDown extends Component {

	//默认参数
	static defaultProps = {
		mid : "dropdownId", //默认box 的 ID
		btn:<a>dropdown</a>,
		modalStyle:{}, //modal的样式
		visible : false, //框默认隐藏
		showBack : null, //显示时候的回调
		closeBack : null, //关闭时候的回调
		trigger : "hover" //触发下拉的行为
	}
	
	//构造函数
	constructor (props) {
		super(props);
		this.state = {
			show: props.visible,
			set: {
				left:null,
				top:null,
				width:null,
				height:null
			}
		}
		this.div = document.createElement('div');
		this.div.setAttribute('class','mt-div');
	}

	//渲染div
	renderDiv(){
		var _this = this;
		var dom = document.getElementById(this.props.mid);
		//首次渲染即可
		if(!dom){
			this.setState({
				set:{
					left: offsetLeft(this.refs.btn),
					top: offsetTop(this.refs.btn),
					width: this.refs.btn.offsetWidth,
					height: this.refs.btn.offsetHeight
				}
			})
 			document.body.appendChild(this.div);
			ReactDOM.render(<DropModal show={this.state.show} {...this.props}/>,this.div);
		}
	}

	//隐藏显示
	showOrHide(show,callback){
		var _this = this;
		this.renderDiv();
		this.setState({
			show: !show,
			set:{
				left: offsetLeft(this.refs.btn),
				top: offsetTop(this.refs.btn),
				width: this.refs.btn.offsetWidth,
				height: this.refs.btn.offsetHeight
			}
		},function(){
			if(_this.state.show){
				_this.props.showBack()
			}else{
				_this.props.closeBack()
			}
			if(callback){
				callback();
			}
		})
		this.hoverHandler = null;
	}

	//更新弹窗里面的数据
	componentDidUpdate(prevProps){
		ReactDOM.render(<DropModal show={this.state.show} set={this.state.set} {...this.props}/>,this.div);
	}

	//点击其他区域，隐藏下拉框
	triggerBlank(events){
		let _this = this;

		var callback = function(){
			document.removeEventListener(events,handler)
		}
		var handler = function(e){
			const self = this;
			if(!closest(e.target,'.mt-dropdown') && !closest(e.target,'.mt-dropdown-btn')){
				_this.showOrHide(true,callback)
			}
		}
		document.removeEventListener(events,handler)
		document.addEventListener(events,handler)
	}

	componentWillUnmount() {
	    this.refs.btn.removeEventListener('mouseenter',this.hoverHandler);
	    this.div.remove();
        ReactDOM.unmountComponentAtNode(this.div);
	}

	//初始化状态
	componentDidMount() {

		//如果默认是显示，直接显示DIV
		if(this.props.visible){
			this.renderDiv();
		}

		//hover事件
		if(this.props.trigger == 'hover'){
			const _this = this;

			this.hoverHandler = function(e){
				_this.showOrHide(false,function(){
					_this.triggerBlank('mousemove')
				})
			}
			this.refs.btn.addEventListener('mouseenter',this.hoverHandler);
		}
	}

	//点击事件
	handleClick(){
		const _this = this;
		if(this.props.trigger == 'click'){
			this.showOrHide(this.state.show,function(){
				_this.triggerBlank('click')
			});
		}else{
			return
		}
	}

	render(){
		var Component = this.props.btn.type;
		var {children,className, ...other} = this.props.btn.props;
		className += ' mt-dropdown-btn';
		return <Component ref="btn" className={className} onClick={this.handleClick.bind(this)} {...other}>{children}</Component>
	}
}

//主页
export default DropDown;