'use strict';

import './datePicker.scss';
import React, { Component, PropTypes } from 'react';
import assign from '../utils/assign';
import {formatDate,addAndDelOneMonth,getMDay,fliterNum,getDateNow} from './dateCore';

class DatePickerBox extends Component {

	//构造函数
	constructor (props) {
		super(props);
		this.state = {
			show : 'day',
			headtime: null,
			yearList : [],
			defaultShow:'day'
		}
	}

	//上一页
	prevClick(){
		console.log('上一页');
		let {year,month,day} = this.props.nowDate;
		var obj = addAndDelOneMonth(year,month,'del');
		obj['day'] = day;
		this.props.resetDays(obj);
		this.setHeadTime(obj);
	}

	//下一页
	nextClick(){
		console.log('下一页');
		let {year,month,day} = this.props.nowDate;
		var obj = addAndDelOneMonth(year,month,'add');
		obj['day'] = day;
		this.props.resetDays(obj);
		this.setHeadTime(obj);
	}

	//上一层的上一页
	prevOutClick(){

		let {year,month,day} = this.props.nowDate;
		let self = this;
		year = parseInt(year, 10);
		let obj = {
			year: this.state.show === 'year' ? year - 30 : --year,
			month: month,
			day:day
		}
		this.setHeadTime(obj);
		this.props.resetDays(obj,function(){
			if(self.state.show === 'year'){
				self.changeBox()
			}
		});
	}

	//上一层的上一页
	nextOutClick(){
		let {year,month,day} = this.props.nowDate;
		let self = this;
		year = parseInt(year, 10);
		let obj = {
			year: this.state.show === 'year' ? year + 30 : ++year,
			month: month,
			day:day
		}
		this.setHeadTime(obj);
		this.props.resetDays(obj,function(){
			if(self.state.show === 'year'){
				self.changeBox()
			}
		});
	}

	iniYear(year){
		year = parseInt(year, 10);
		let yearList = [];
		for(let i = year - 10; i < year + 20; i++){
			yearList.push({
				year: i,
				type: 'year',
				active: i === year ? true : false
			})
		}
		this.setState({
			show: 'year',
			headtime: ( year - 10) + '-' + (year + 19),
			yearList: yearList
		})
	}

	//切换年，月，日的选择
	changeBox(){
		if(this.state.show == 'day'){
			this.setState({
				show: 'month',
				headtime: formatDate(this.props.nowDate,'yyyy').val
			})
		}else{ // if(this.state.show == 'month')
			let year = formatDate(this.props.nowDate,'yyyy').val;
			this.iniYear(year);
		}
	}

	//设置年
	clickYear(elem){

		if(elem.active){
			return
		}

		//日这里需要做判断，年份变化，2月可能没有29号
		let maxDay = getMDay(elem.year,elem.month);
		let {day, month} = this.props.nowDate;
		this.props.resetDays({
			year:elem.year,
			month:month,
			day:day > maxDay ? maxDay : day
		})

		//没有月份的选择了
		if(this.state.defaultShow == 'year'){
			this.iniYear(elem.year);
		}else{
			this.setState({
				show:'month'
			},function(){
				this.setHeadTime(elem);
			}.bind(this))
		}
	}

	//选择月
	clickMonth(elem){
		this.setHeadTime(elem)
		if(elem.active){
			return
		}
		//日这里需要做判断，年份变化，2月可能没有29号
		let maxDay = getMDay(elem.year,elem.month);
		let {day} = this.props.nowDate;
		this.props.resetDays({
			year:elem.year,
			month:elem.month,
			day:day > maxDay ? maxDay : day
		});

		//如果没有天的选择
		if(this.state.defaultShow == 'month'){
			this.setHeadTime(elem);
		}else{
			this.setState({
				show:'day'
			},function(){
				this.setHeadTime(elem);
			}.bind(this))
		}
	}

	//选择天
	clickDay(elem){

		let maxDay = getMDay(elem.year,elem.month);
		this.props.resetDays({
			year:elem.year,
			month:elem.month,
			day:elem.day > maxDay ? maxDay : elem.day
		})
		//console.log('DatePickerBox 134 >> 当前选择的日期：',elem)
	}

	//设置show,和 headtime
	setHeadTime(obj){
		if(this.state.show == 'day'){
			this.setState({
				headtime: obj.year + '-' + fliterNum(obj.month)
			})
		}else if(this.state.show == 'month'){
			this.setState({
				headtime: obj.year
			})
		}
	}

	//判断年，月，日的选项
	dateForFormat(){
		var obj = formatDate(this.props.nowDate,this.props.format);
		//如果没有天的选择
		if(!obj.show.day){

			//如果又没有月份
			if(!obj.show.month){
				this.setState({
					show: 'year',
					defaultShow:'year'
				})
				this.iniYear(obj.val);
			}else{
				this.setState({
					show: 'month',
					defaultShow:'month'
				})
			}
		}
	}

	//初始化
	componentDidMount() {
	 	this.setState({
	 		headtime: formatDate(this.props.nowDate,'yyyy-mm').val
	 	})

	 	this.dateForFormat();  
	}

	//今天
	clickNowDay(str){
		let obj = getDateNow();
		this.props.resetDays({
			year:obj.year,
			month:obj.month,
			day:obj.day
		})
	}

	//确定
	clickOk(str){
		this.props.showOrHide(true,null,{
			str:formatDate(this.props.nowDate,'yyyy-mm-dd').val,
			obj:this.props.nowDate,
			clear: str === 'clear' ? true : false
		})
	}

	//清除
	clearDate(){
		let self = this;
		this.clickOk('clear')
	}

	render(){

		//渲染日历
		var {years,months,days,hours,minutes,seconds} = this.props.date;
		var self = this;
		//设置定位
		if(this.props.set){
			let {height, left, top, width} = this.props.set;

			var style = assign([{
				left : left + 230 > document.body.offsetWidth ? document.body.offsetWidth - 230 - 10 : left,//判断left,不能超过body区域
				top : top + height
			},this.props.modalStyle,{display: this.props.show?'block':'none'}])
		}
		
		return <div className={'mt-date '+this.props.modalClassName} ref="datePicker" id={this.props.mid} style={style}>
					<div className="mt-date-head">
						<a onClick={this.prevOutClick.bind(this)} className="mt-date-btn-prev"><i className="iconfont icon-arrow3l"></i></a>
						{ this.state.show === 'day'? <a onClick={this.prevClick.bind(this)} className="mt-date-btn-prev"><i className="iconfont icon-arrowl"></i></a> : null }
						<span onClick={this.changeBox.bind(this)} className="mt-date-stime">{this.state.headtime}</span>
						{ this.state.show === 'day'? <a onClick={this.nextClick.bind(this)} className="mt-date-btn-next"><i className="iconfont icon-arrowr"></i></a> : null }
						<a onClick={this.nextOutClick.bind(this)} className="mt-date-btn-next"><i className="iconfont icon-arrow3r"></i></a>
					</div>
					<div className="mt-date-body-years" style={{display: this.state.show === 'year'?'block':'none'}}>
						<div className="mt-date-year">
							<ul className="clearfix">
								{
									this.state.yearList.map(function(elem, index) {
										return <li onClick={self.clickYear.bind(self,elem)} key={index} className={"mt-date-yearli" + (elem.active?' mt-date-active':'')}><a>{elem.year}</a></li>;
									})
								}
							</ul>
						</div>
					</div>
					<div className="mt-date-body-months" style={{display: this.state.show === 'month'?'block':'none'}}>
						<div className="mt-date-month">
							<ul className="clearfix">
								{
									this.props.date.months.map(function(elem, index) {
										return <li onClick={self.clickMonth.bind(self,elem)} key={index} className={"mt-date-month-item" + (elem.active?' mt-date-active':'')}><a>{elem.month}月</a></li>;
									})
								}
							</ul>
						</div>
					</div>
					<div className="mt-date-body-days" style={{display: this.state.show === 'day'?'block':'none'}}>
						<div className="mt-date-week">
							<ul>
								{
									this.props.setWeek.map(function(elem, index) {
										return <li key={index}>{elem}</li>;
									})
								}
							</ul>
						</div>
						<div className="mt-date-day">
							<ul className="clearfix">
								{
									this.props.date.days.map(function(elem, index) {
										return <li onClick={self.clickDay.bind(self,elem)} key={index} className={"mt-date-day-" + elem.type + (elem.active?' mt-date-active':'')}><a>{elem.day}</a></li>;
									})
								}
							</ul>
						</div>
					</div>
					<div className="mt-date-foot">
						<a onClick={this.clickNowDay.bind(this)} className="mt-date-btn-now">今天</a>
						<a className="mt-date-btn-time">时钟</a>
						<a onClick={this.clickOk.bind(this)} className="mt-btn-xs mt-btn-primary mt-date-btn-ok">确定</a>
						<a onClick={this.clearDate.bind(this)} className="mt-btn-xs mt-btn-warning mt-date-btn-clear">清除</a>
					</div>
			   </div>
	}
}

//主页
export default DatePickerBox;