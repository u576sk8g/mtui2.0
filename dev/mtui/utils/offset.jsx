/**
* @type 获取dom 行对body的left
* @author : Mantou
*/
function offsetLeft(self) {
    var val = 0;
    while(self){
        val += self.offsetLeft;
        self = self.offsetParent;
    }
    return val;
}

function offsetTop(self) {
    var val = 0;
    var i =0;
    while(self){
        val += self.offsetTop;
        self = self.offsetParent;
    }
    return val;
}

export {offsetTop,offsetLeft};