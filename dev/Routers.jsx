'use strict';
import React, { Component } from 'react';
import { Router, Route, IndexRoute,IndexRedirect} from 'react-router'; // 路由
import { browserHistory } from 'react-router';

//首页
import Index from './Pages/Index/Index';

//帮助中心
import Help from './Pages/Help/Index';

//redux案例展示
import NotFound from './Pages/Common/NotFound'; 
import ReduxDom from './Pages/ReduxDom/ReduxDom'; 
import UI from './Pages/UI/Index'; 

//UI DEMO
import InputDom from './Pages/UI/Input'
import ButtonDom from './Pages/UI/Button'
import SwitchDom from './Pages/UI/Switch'
import SliderDom from './Pages/UI/Slider'
import SliderBarDom from './Pages/UI/SliderBar'
import SelectDom from './Pages/UI/Select'
import RadioDom from './Pages/UI/Radio'
import DatePickerDom from './Pages/UI/DatePicker'
import CheckboxDom from './Pages/UI/Checkbox'
import GridDom from './Pages/UI/Grid'
import PanleDom from './Pages/UI/panle'
import PageListDom from './Pages/UI/Pagelist'
import TipDom from './Pages/UI/Tip'
import ModalDom from './Pages/UI/Modal'
import DropdownDom from './Pages/UI/Dropdown'
import BacktopDom from './Pages/UI/Backtop'
import TabsDom from './Pages/UI/Tabs'
import Page404Dom from './Pages/UI/Page404'

import TagDom from './Pages/UI/Tag'
import PopoverDom from './Pages/UI/Popover'
import LoadingDom from './Pages/UI/Loading'
import LoadingBarDom from './Pages/UI/LoadingBar'
import TreeDom from './Pages/UI/Tree'
import UploadDom from './Pages/UI/Upload'
import TableDom from './Pages/UI/Table'
import CollapseDom from './Pages/UI/Collapse'
import PopconfirmDom from './Pages/UI/Popconfirm'
import ValidateDom from './Pages/UI/Validate'
import SwiperDom from './Pages/UI/Swiper'

//App为入口
import App from './Pages/App';

class Routers extends Component{
	leavePath(){
		//回收echart残留的内存开销 
        if(ECHART_ROOM.length > 0){
           for(var i=0; i< ECHART_ROOM.length; i++){
              echarts.dispose(ECHART_ROOM[i]);
              ECHART_ROOM.pop();
           }
        }
	}
	enterPath(){
		document.body.scrollTop = 0;
	}
    render() {
        return (
            <Router history={this.props.history}>
			    <Route onEnter={this.enterPath} onLeave={this.leavePath} path={HOME} component={App}>
			      <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"/"} component={Index} />
			      <IndexRoute onEnter={this.enterPath} onLeave={this.leavePath} component={Index} />
			      <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"ui"} component={UI}>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"input"} component={InputDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"button"} component={ButtonDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"switch"} component={SwitchDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"slider"} component={SliderDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"sliderbar"} component={SliderBarDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"select"} component={SelectDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"radio"} component={RadioDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"checkbox"} component={CheckboxDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"datepicker"} component={DatePickerDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"grid"} component={GridDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"panle"} component={PanleDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"pagelist"} component={PageListDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"tip"} component={TipDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"modal"} component={ModalDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"dropdown"} component={DropdownDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"backtop"} component={BacktopDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"tabs"} component={TabsDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"tag"} component={TagDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"popover"} component={PopoverDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"loading"} component={LoadingDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"loadingbar"} component={LoadingBarDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"tree"} component={TreeDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"upload"} component={UploadDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"table"} component={TableDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"collapse"} component={CollapseDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"popconfirm"} component={PopconfirmDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"validate"} component={ValidateDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"swiper"} component={SwiperDom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"404"} component={Page404Dom}/>
			          <Route onEnter={this.enterPath} onLeave={this.leavePath} path={"redux"} component={ReduxDom}/>
			      </Route>
			      <Route onEnter={this.enterPath} onLeave={this.leavePath} path="*" component={NotFound}/>
			    </Route>
		  	</Router>
        );
    }
};

export default Routers;