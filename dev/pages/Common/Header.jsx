'use strict';
import './style.scss';
import React, { Component } from 'react';
import { Link,browserHistory } from 'react-router';
import { connect} from 'react-redux'

class Header extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return <div className="header">
			基于React的一套轻量级的组件库 {this.props.tips}
		</div>;
	}
}
export default connect(
  state => ({ 
    tips: state.user.tips
  }),null
  )(Header);