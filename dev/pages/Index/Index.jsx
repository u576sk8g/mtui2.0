'use strict';

import './style.scss';
import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class Index extends Component {
	//构造函数
	constructor (props) {
		super(props);
	}

	render(){
		return (
	        <div className="index">
	        	<h1>MTUI2.0 Beta</h1>
	        	<div className="btns">
	        		<Link to={HOME+"/ui/input"}></Link>
	        		&nbsp;
	        		<Link to={HOME+"/redux"}></Link>
	        	</div>
	        </div>
	    );
	}
}

//主页
export default Index;