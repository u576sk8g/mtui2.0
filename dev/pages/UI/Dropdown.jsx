'use strict';

import './style.scss';
import React, { Component, PropTypes } from 'react';
import {Grid,Button,Dropdown,Panle} from '../../mtui/index'

class UI extends Component {
	//构造函数
	constructor (props) {
		super(props);
		this.state = {
			name : 10
		}
	}

	closeBack(){
		console.log('close')
	}

	showBack(){
		const _this = this;
		console.log('show')
		_this.setState({
			name: ++this.state.name
		})
	}

	render(){
		return (

			<Panle header="Dropdown 弹窗">
				<Dropdown mid="dpdtest" 
	        		btn={<a className="mt-btn mt-btn-success"><i>下拉框hover</i></a>} 
	        		modalStyle={{width:200, height:200}} 
	        		visible={false} 
	        		showBack={this.showBack.bind(this)} 
	        		closeBack={this.closeBack.bind(this)} trigger="hover">
	        			<Panle header="弹窗">
	        				{this.state.name}
	        			</Panle>
	        		</Dropdown>

	        		&nbsp;
	        		&nbsp;

	        		<Dropdown mid="dpdtest2" 
	        		btn={<a className="mt-btn mt-btn-info"><i>下拉框click</i></a>} 
	        		modalStyle={{width:100, height:100}} 
	        		showBack={this.showBack.bind(this)} 
	        		closeBack={this.closeBack.bind(this)} trigger="click">
	        			<Panle size="min" header="弹窗2">
	        				{this.state.name}
	        			</Panle>
	        		</Dropdown>

<div>
<pre>
<code>
{`
import './style.scss';
import React, { Component, PropTypes } from 'react';
import {Grid,Button,Dropdown} from '../../mtui/index'

class UI extends Component {
	//构造函数
	constructor (props) {
		super(props);
		this.state = {
			name : 10
		}
	}

	closeBack(){
		console.log('close')
	}

	showBack(){
		const _this = this;
		console.log('show')
		_this.setState({
			name: ++this.state.name
		})
	}

	render(){
		return (
			<div className="mt-panle">
	        	<div className="mt-panle-h2">DropDown弹窗</div>
	        	<div className="mt-panle-box">
	        		<Dropdown mid="dpdtest" 
	        		btn={<a className="mt-btn mt-btn-success"><i>下拉框</i></a>} 
	        		modalStyle={{width:200, height:100}} 
	        		visible={false} 
	        		showBack={this.showBack.bind(this)} 
	        		closeBack={this.closeBack.bind(this)} trigger="hover">
	        			<div className="mt-panle-min">
	        				<div className="mt-panle-h2">标题1</div>
	        				<div className="mt-panle-box">{this.state.name}...</div>
	        			</div>
	        		</Dropdown>
	        	</div>
	        </div>
	    );
	}
}
`}
</code>
</pre>
</div>

			</Panle>
	    );
	}
}

//主页
export default UI;