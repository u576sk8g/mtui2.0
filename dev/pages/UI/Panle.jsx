'use strict';

import './style.scss';
import React, { Component } from 'react';
import {Grid,Panle} from '../../mtui/index'

class UI extends Component {
	//构造函数
	constructor (props) {
		super(props);
	}

	render(){
		return (
			<Grid width="1/1">
				<Grid width="1/2" sm="2/2">
					<Panle header="面板1">
						这是一个响应式案例，缩小到640px试试<br/>
			          	.mt-panle<br/>
			          	.mt-panle-h2<br/>
			          	.mt-panle-box<br/>
					</Panle>	
		        </Grid>
		        <Grid width="1/2" sm="2/2">
		          	<Panle header="面板2">
						这是一个响应式案例，缩小到640px试试<br/>
			          	.mt-panle<br/>
			          	.mt-panle-h2<br/>
			          	.mt-panle-box<br/>
					</Panle>
		        </Grid>
		        <Grid width="1/1">
		        	<Panle size="min" header="面板3">
		        		小面板
					</Panle>
		        </Grid>
<div>
<pre>
<code>
{`
'use strict';

import './style.scss';
import React, { Component } from 'react';
import {Grid,Panle} from '../../mtui/index'

class UI extends Component {
	//构造函数
	constructor (props) {
		super(props);
	}

	render(){
		return (
			<Grid width="1/1">
				<Grid width="1/2" sm="2/2">
					<Panle header="面板1">
						这是一个响应式案例，缩小到640px试试<br/>
			          	.mt-panle<br/>
			          	.mt-panle-h2<br/>
			          	.mt-panle-box<br/>
					</Panle>	
		        </Grid>
		        <Grid width="1/2" sm="2/2">
		          	<Panle header="面板2">
						这是一个响应式案例，缩小到640px试试<br/>
			          	.mt-panle<br/>
			          	.mt-panle-h2<br/>
			          	.mt-panle-box<br/>
					</Panle>
		        </Grid>
		        <Grid width="1/1">
		        	<Panle size="min" header="面板3">
		        		小面板
					</Panle>
		        </Grid>
	        </Grid>
	    );
	}
}
`}
</code>
</pre>
</div>
	        </Grid>
	    );
	}
}

//主页
export default UI;