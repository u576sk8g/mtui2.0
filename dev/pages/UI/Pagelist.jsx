'use strict';

import './style.scss';
import React, { Component, PropTypes } from 'react';
import {PageList,Grid} from '../../mtui/index'

class UI extends Component {
	//构造函数
	constructor (props) {
		super(props);
		this.state = {
		  total:0
		};
		this.timer = null
	}

	//分页点击后执行
	callback(obj){
		console.log('分页回调',obj)
	}

	componentWillUnmount() {
	    if(this.timer){
	 		clearTimeout(this.timer)   
		}
	}

	componentDidMount() {
		let _this = this;
		this.timer = setTimeout(function(){
			_this.setState({
				total:200
			})
		},1)
	}

	render(){
		return (
	        <div className="mt-panle">
        		<h3 className="mt-panle-h2">分页</h3>
        		<div className="mt-panle-box">
	        		<Grid width='2/2'>
	        			<PageList current={1} pageSize={15} callback={this.callback} total={this.state.total}/>
	        		</Grid>
	        		<Grid width='1/1' className="code">
<pre>
<code>
{`
import './style.scss';
import React, { Component, PropTypes } from 'react';
import {PageList} from '../../mtui/index'

class UI extends Component {
	//构造函数
	constructor (props) {
		super(props);
		this.state = {
		  total:0
		};
		this.timer = null
	}

	//分页点击后执行
	callback(obj){
		console.log('分页回调',obj)
	}

	componentWillUnmount() {
	    if(this.timer){
	 		clearTimeout(this.timer)   
		}
	}

	componentDidMount() {
		let _this = this;
		this.timer = setTimeout(function(){
			_this.setState({
				total:200
			})
		},1)
	}

	render(){
		return (
	        <PageList current={1} pageSize={15} callback={this.callback} total={this.state.total}/>
	    );
	}
}
`}
</code>
</pre>	        			
	        		</Grid>
        		</div>
	        </div>
	    );
	}

	//
}

//主页
export default UI;