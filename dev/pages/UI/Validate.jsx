'use strict';

import './style.scss';
import React, { Component } from 'react';
import {Grid,Panle,Validate,Input,Button} from '../../mtui/index'

const ValidateGroup = Validate.ValidateGroup;

class UI extends Component {
	//构造函数
	constructor (props) {
		super(props);
	}

	onChange(e){
		console.log('>>',e.target.value)
	}

	onEnd(data){
		console.log("submit=>",data)
	}

	render(){
		return (
			<Panle header="Validate">
				<ValidateGroup submit={this.onEnd}>
					<div>
						<Validate exgs={[
							{regs:'notempty',type:'warning',info:'不能为空！'},
							{regs:'email',type:'danger',info:'邮箱错误！'}
						]}><Input onChange={this.onChange.bind(this)} placeholder="邮箱"/></Validate>
					</div>
					<br/>
					<div>
						<Validate exgs={[
							{regx:'^[2-6]+$',type:'danger',info:'请输入2~6的数字'}
						]}><Input onChange={this.onChange.bind(this)} placeholder="请输入2~6的数字"/></Validate>
					</div>
					<br/>
					<div>
						表单验证案例
						<br/>regs 可用参数请参考 mtui/Validate/regex   
						<br/>regx: 自定义验证规则
					</div>
					<br/>
					<Button style={{width:181}} dom="button" htmlType="submit" type="primary" className="login-form-button">submit</Button>
				</ValidateGroup>
				<pre><code>
				{`
'use strict';

import './style.scss';
import React, { Component } from 'react';
import {Grid,Panle,Validate,Input} from '../../mtui/index'

const ValidateGroup = Validate.ValidateGroup;

class UI extends Component {
	//构造函数
	constructor (props) {
		super(props);
	}

	onChange(e){
		console.log('>>',e.target.value)
	}

	render(){
		return (
			<Panle header="Validate">
				<ValidateGroup submit={this.onEnd}>
					<div>
						<Validate exgs={[
							{regs:'notempty',type:'warning',info:'不能为空！'},
							{regs:'email',type:'danger',info:'邮箱错误！'}
						]}><Input onChange={this.onChange.bind(this)} placeholder="邮箱"/></Validate>
					</div>
					<br/>
					<div>
						<Validate exgs={[
							{regx:'^[2-6]+$',type:'danger',info:'请输入2~6的数字'}
						]}><Input onChange={this.onChange.bind(this)} placeholder="请输入2~6的数字"/></Validate>
					</div>
					<br/>
					<div>表单验证案例</div>
					<br/>
					<Button style={{width:181}} dom="button" htmlType="submit" type="primary" className="login-form-button">submit</Button>
				</ValidateGroup>
			</Panle>	
	    );
	}
}
				`}
				</code></pre>
			</Panle>	
	    );
	}
}

//主页
export default UI;