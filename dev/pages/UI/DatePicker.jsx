'use strict';

import './style.scss';
import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Grid,Button,DatePicker,Input} from '../../mtui/index'

class Dates extends Component {
	//构造函数
	constructor (props) {
		super(props);
		this.state = {
			name : '111'
		}
	}

	onChange(data){
		console.log('弹窗关闭',data);
	}

	render(){
		return (
	        <div className="mt-panle">
	        	<div className="mt-panle-h2">日期组件</div>
	        	<div className="mt-panle-box">
	        		<DatePicker size="xs" style={{width:80}} defaultValue="" onChange={this.onChange.bind(this)} mid="dateId" format="yyyy-mm-dd" placeholder="选择日期" />&nbsp;
	        		<DatePicker defaultValue="2017-03" onChange={this.onChange.bind(this)} mid="dateId2" format="yyyy-mm"/>&nbsp;
	        		<DatePicker defaultValue="2017" onChange={this.onChange.bind(this)} mid="dateId3" format="yyyy"/>&nbsp;
	        	</div>
	        	<Grid className="code" width="2/2">
<pre>
<code>
{`
import './style.scss';
import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Grid,Button,DatePicker,Input} from '../../mtui/index'

class Dates extends Component {
	//构造函数
	constructor (props) {
		super(props);
		this.state = {
			name : '111'
		}
	}

	onChange(data){
		console.log('弹窗关闭',data);
	}

	render(){
		return (
	        <div>
        		<DatePicker size="xs" style={{width:80}} defaultValue="" onChange={this.onChange.bind(this)} mid="dateId" format="yyyy-mm-dd" placeholder="选择日期" />&nbsp;
        		<DatePicker defaultValue="2017-03" onChange={this.onChange.bind(this)} mid="dateId2" format="yyyy-mm"/>&nbsp;
        		<DatePicker defaultValue="2017" onChange={this.onChange.bind(this)} mid="dateId3" format="yyyy"/>&nbsp;
        	</div>
	    );
	}
}
`}
</code>
</pre>
	        	</Grid>
	        </div>
	    );
	}
}

//主页
export default Dates;