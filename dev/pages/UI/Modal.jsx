'use strict';

import './style.scss';
import React, { Component, PropTypes } from 'react';
import {Modal,Grid} from '../../mtui/index'

class UI extends Component {
    //构造函数
    constructor (props) {
        super(props);
        this.state = {
            name : '111'
        }
    }

    showBack() {
        console.log('弹窗开启，2秒后，自动变化值！')
        var _this = this;
        setTimeout(function(){
            _this.setState({
                name: '2222'
            })
        },2000)  
    }

    closeBack(){
        console.log('弹窗关闭');
    }

    render(){
        return (
            <div className="mt-panle">
                <div className="mt-panle-h2">Modal弹窗</div>
                <div className="mt-panle-box">
                    <Modal mid="modalID" btn={<a className="mt-btn mt-btn-success"><i>弹窗</i></a>} modalClassName="animated bounceInDown" modalStyle={{width:200, height:180}} showBack={this.showBack.bind(this)} closeBack={this.closeBack.bind(this)}>
                        <div className="mt-panle-min">
                            <div className="mt-panle-h2">标题</div>
                            <div className="mt-panle-box">{this.state.name}内容...</div>
                        </div>
                    </Modal>
<pre>
<code>
{`
import './style.scss';
import React, { Component, PropTypes } from 'react';
import {Modal} from '../../mtui/index';

class UI extends Component {
	//构造函数
	constructor (props) {
	    super(props);
	    this.state = {
	        name : '111'
	    }
	}

	showBack() {
	    alert('弹窗开启，2秒后，自动变化值！')
	    var _this = this;
	    setTimeout(function(){
	        _this.setState({
	            name: '2222'
	        })
	    },2000)  
	}

	closeBack(){
	    console.log('弹窗关闭');
	}

	render(){
	    return{
	        <Modal 
	            mid="modalID" 
	            modalClassName="animated bounceInDown" 
	            modalStyle={{width:200, height:180}} 
	            showBack={this.showBack.bind(this)} 
	            closeBack={this.closeBack.bind(this)}> 
	            btn={<a className="mt-btn mt-btn-success"><i>弹窗</i></a>} 
	            <div className="mt-panle-min">
	                <div className="mt-panle-h2">标题</div>
	                <div className="mt-panle-box">{this.state.name}内容...</div>
	            </div>
	        </Modal>
	    }
	}
}
`}
</code>
</pre>
                </div>
                <Grid className="code" width="2/2">
                </Grid>
            </div>
        );
    }
}

//主页
export default UI;