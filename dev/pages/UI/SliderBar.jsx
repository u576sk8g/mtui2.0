'use strict';

import './style.scss';
import React, { Component, PropTypes } from 'react';
import {Grid,Button,Panle,SliderBar} from '../../mtui/index'

class Dom extends Component {
	//构造函数
	constructor (props) {
		super(props);
	}

	onChange(data){
		console.log(data)
	}

	sliderEnd(data){
		console.log(data)
	}

	render(){
		return (
	        <Panle header="sliderBar">
        		<SliderBar type="danger" width={300} value={0.5} /> <br/><br/>
        		<SliderBar type="success" width={300} value={0.6} /> <br/><br/>
        		<SliderBar type="default" width={300} value={0.7} /> <br/><br/>
        		<SliderBar type="primary" width={300} value={0.8} /> <br/><br/>
        		<SliderBar type="info" width={300} value={0.9} /> <br/><br/>
	        	<div>
					<pre>
					<code>
{`

import './style.scss';
import React, { Component, PropTypes } from 'react';
import {Grid,Button,Panle,SliderBar} from '../../mtui/index'

class Dom extends Component {
	//构造函数
	constructor (props) {
		super(props);
	}

	render(){
		return (
	        <Panle header="slider">
        		<SliderBar type="danger" width={300} value={0.5} /> <br/><br/>
        		<SliderBar type="success" width={300} value={0.6} /> <br/><br/>
        		<SliderBar type="default" width={300} value={0.7} /> <br/><br/>
        		<SliderBar type="primary" width={300} value={0.8} /> <br/><br/>
        		<SliderBar type="info" width={300} value={0.9} /> <br/><br/>
	        </Panle>
	    );
	}
}
`}
					</code>
					</pre>
	        	</div>
	        </Panle>
	    );
	}
}

//主页
export default Dom;