var webpack = require('webpack');
var path = require('path');
var HtmlwebpackPlugin = require('html-webpack-plugin');
//var CopyWebpackPlugin = require('copy-webpack-plugin');
//var CleanPlugin = require('clean-webpack-plugin')//webpack插件，用于清除目录文件
var ROOT_PATH = path.resolve(__dirname);
var port = 4000;
var host = '127.0.0.1'
module.exports = {
    //页面入口文件配置
    entry: [
        'webpack-dev-server/client?http://'+host+':'+port,//资源服务器
        'webpack/hot/only-dev-server',
        path.resolve(ROOT_PATH, 'dev/index_dev.jsx'),
    ],
    //入口文件输出配置
    output: {
        'path': path.resolve(ROOT_PATH, 'build'),
        //'publicPath': 'build',// 网站运行时的访问路径
        'filename': '/assets/js/index.js'
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.scss', '.css'] //require 的时候，可以不用写文件类型
    },
    //插件项
    plugins: [//将外部的包导出成一个公用的文件比如 jquery，react, react-dom 等
        new HtmlwebpackPlugin({
            title: 'mtui',
            template:'./dev/index.html',//'./dev/index.html', //html模板路径
            filename: 'index.html',
            inject:true,  //允许插件修改哪些内容，包括head与body
            hash:false //为静态资源生成hash值
        }),//添加我们的插件 会自动生成一个html文件
        //new CleanPlugin(['./build/*.hot-update.js','./build/*.hot-update.json']),
        //把指定文件夹xia的文件复制到指定的目录
        // new CopyWebpackPlugin([{
        //     from: 'dev/assets/libs/css/animate.css',
        //     to: 'build'
        // }]),
        new webpack.DefinePlugin({ //压缩 React
          "process.env": {
            NODE_ENV: JSON.stringify("development") //development,production
          }
        }),
        new webpack.NoErrorsPlugin(), //启用报错不打断模式
        new webpack.HotModuleReplacementPlugin() //热替换插件
    ],
    module: {
        //加载器配置
        //凡是.js结尾的文件都是用babel-loader做处理，而.jsx结尾的文件会先经过jsx-loader处理，然后经过babel-loader处理
        loaders: [
            {
                test:/\.jsx?$/,
                exclude:/(node_modules)/,
                // 在这里添加 react-hot，注意这里使用的是loaders，所以不能用 query，应该把presets参数写在 babel 的后面
                loaders: ["babel-loader"]//loaders: ['react-hot', 'babel?presets[]=react,presets[]=es2015']
            },
            {test: /\.(woff|eot|ttf)$/i, loader: 'file-loader?name=./assets/fonts/[name].[ext]'},
            {test: /\.scss$/, loader: "style!css!sass"},
            // {test: /\.less$/, loader: "style!css!less"},
            {test:/\.css$/, loader: "style!css"},
            {test: /\.(png|jpg|gif)$/, loader: "file-loader?name=./assets/imgs/[name].[ext]"}
        ]
    },
    devServer: {
        contentBase: path.resolve(ROOT_PATH, 'build'),
        historyApiFallback: true,
        hot: true,
        inline:true,
        progress: true,
        stats: 'error-only',
        host: host,
        port: port
      }
};
