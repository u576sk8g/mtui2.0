'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

require('./dropDown.scss');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _DropModal = require('./DropModal');

var _DropModal2 = _interopRequireDefault(_DropModal);

var _select = require('../utils/select');

var _offset = require('../utils/offset');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DropDown = function (_Component) {
	(0, _inherits3.default)(DropDown, _Component);

	//构造函数
	function DropDown(props) {
		(0, _classCallCheck3.default)(this, DropDown);

		var _this2 = (0, _possibleConstructorReturn3.default)(this, (DropDown.__proto__ || (0, _getPrototypeOf2.default)(DropDown)).call(this, props));

		_this2.state = {
			show: props.visible,
			set: {
				left: null,
				top: null,
				width: null,
				height: null
			}
		};
		_this2.div = document.createElement('div');
		_this2.div.setAttribute('class', 'mt-div');
		return _this2;
	}

	//渲染div


	//默认参数


	(0, _createClass3.default)(DropDown, [{
		key: 'renderDiv',
		value: function renderDiv() {
			var _this = this;
			var dom = document.getElementById(this.props.mid);
			//首次渲染即可
			if (!dom) {
				this.setState({
					set: {
						left: (0, _offset.offsetLeft)(this.refs.btn),
						top: (0, _offset.offsetTop)(this.refs.btn),
						width: this.refs.btn.offsetWidth,
						height: this.refs.btn.offsetHeight
					}
				});
				document.body.appendChild(this.div);
				_reactDom2.default.render(_react2.default.createElement(_DropModal2.default, (0, _extends3.default)({ show: this.state.show }, this.props)), this.div);
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			//document.getElementById(this.props.mid); 
			this.div.remove();
			_reactDom2.default.unmountComponentAtNode(this.div);
		}

		//隐藏显示

	}, {
		key: 'showOrHide',
		value: function showOrHide(show, callback) {
			var _this = this;
			this.renderDiv();
			this.setState({
				show: !show,
				set: {
					left: (0, _offset.offsetLeft)(this.refs.btn),
					top: (0, _offset.offsetTop)(this.refs.btn),
					width: this.refs.btn.offsetWidth,
					height: this.refs.btn.offsetHeight
				}
			}, function () {
				if (_this.state.show) {
					_this.props.showBack();
				} else {
					_this.props.closeBack();
				}
				if (callback) {
					callback();
				}
			});
			this.hoverHandler = null;
		}

		//更新弹窗里面的数据

	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(prevProps) {
			_reactDom2.default.render(_react2.default.createElement(_DropModal2.default, (0, _extends3.default)({ show: this.state.show, set: this.state.set }, this.props)), this.div);
		}

		//点击其他区域，隐藏下拉框

	}, {
		key: 'triggerBlank',
		value: function triggerBlank(events) {
			var _this = this;

			var callback = function callback() {
				document.removeEventListener(events, handler);
			};
			var handler = function handler(e) {
				var self = this;
				if (!(0, _select.closest)(e.target, '.mt-dropdown') && !(0, _select.closest)(e.target, '.mt-dropdown-btn')) {
					_this.showOrHide(true, callback);
				}
			};
			document.removeEventListener(events, handler);
			document.addEventListener(events, handler);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			this.refs.btn.removeEventListener('mouseenter', this.hoverHandler);
		}

		//初始化状态

	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {

			//如果默认是显示，直接显示DIV
			if (this.props.visible) {
				this.renderDiv();
			}

			//hover事件
			if (this.props.trigger == 'hover') {
				var _this = this;

				this.hoverHandler = function (e) {
					_this.showOrHide(false, function () {
						_this.triggerBlank('mousemove');
					});
				};
				this.refs.btn.addEventListener('mouseenter', this.hoverHandler);
			}
		}

		//点击事件

	}, {
		key: 'handleClick',
		value: function handleClick() {
			var _this = this;
			if (this.props.trigger == 'click') {
				this.showOrHide(this.state.show, function () {
					_this.triggerBlank('click');
				});
			} else {
				return;
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var Component = this.props.btn.type;
			var _props$btn$props = this.props.btn.props,
			    children = _props$btn$props.children,
			    className = _props$btn$props.className,
			    other = (0, _objectWithoutProperties3.default)(_props$btn$props, ['children', 'className']);

			className += ' mt-dropdown-btn';
			return _react2.default.createElement(
				Component,
				(0, _extends3.default)({ ref: 'btn', className: className, onClick: this.handleClick.bind(this) }, other),
				children
			);
		}
	}]);
	return DropDown;
}(_react.Component);

//主页


DropDown.defaultProps = {
	mid: "dropdownId", //默认box 的 ID
	btn: _react2.default.createElement(
		'a',
		null,
		'dropdown'
	),
	modalStyle: {}, //modal的样式
	visible: false, //框默认隐藏
	showBack: null, //显示时候的回调
	closeBack: null, //关闭时候的回调
	trigger: "hover" //触发下拉的行为
};
var _default = DropDown;
exports.default = _default;
;

var _temp = function () {
	if (typeof __REACT_HOT_LOADER__ === 'undefined') {
		return;
	}

	__REACT_HOT_LOADER__.register(DropDown, 'DropDown', 'dev/mtui/dropdown/Dropdown.jsx');

	__REACT_HOT_LOADER__.register(_default, 'default', 'dev/mtui/dropdown/Dropdown.jsx');
}();

;
//# sourceMappingURL=Dropdown.js.map