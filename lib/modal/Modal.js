'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

require('./modal.scss');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _ModalBox = require('./ModalBox');

var _ModalBox2 = _interopRequireDefault(_ModalBox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Modal = function (_Component) {
	(0, _inherits3.default)(Modal, _Component);

	//构造函数
	function Modal(props) {
		(0, _classCallCheck3.default)(this, Modal);

		var _this2 = (0, _possibleConstructorReturn3.default)(this, (Modal.__proto__ || (0, _getPrototypeOf2.default)(Modal)).call(this, props));

		_this2.state = {
			show: props.visible
		};
		_this2.div = document.createElement('div');
		_this2.div.setAttribute('class', 'mt-div');
		return _this2;
	}

	//默认参数


	(0, _createClass3.default)(Modal, [{
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			//document.getElementById(this.props.mid); 
			this.div.remove();
			_reactDom2.default.unmountComponentAtNode(this.div);
		}

		//渲染div

	}, {
		key: 'renderDiv',
		value: function renderDiv() {
			var _this = this;
			var dom = document.getElementById(this.props.mid);
			//首次渲染即可
			if (!dom) {
				document.body.appendChild(this.div);
				_reactDom2.default.render(_react2.default.createElement(_ModalBox2.default, (0, _extends3.default)({ show: this.state.show, showOrHide: this.showOrHide.bind(this) }, this.props)), this.div); //
			}
		}

		//隐藏显示

	}, {
		key: 'showOrHide',
		value: function showOrHide(show, callback) {
			var _this = this;
			this.renderDiv();
			this.setState({
				show: !show
			}, function () {
				if (_this.state.show) {
					_this.props.showBack();
				} else {
					_this.props.closeBack();
				}
				if (callback) {
					callback();
				}
			});
		}

		//更新弹窗里面的数据

	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(prevProps) {
			_reactDom2.default.render(_react2.default.createElement(_ModalBox2.default, (0, _extends3.default)({ show: this.state.show, showOrHide: this.showOrHide.bind(this), set: this.state.set }, this.props)), this.div);
		}

		//点击事件

	}, {
		key: 'handleClick',
		value: function handleClick() {
			var _this = this;
			this.showOrHide(this.state.show);
		}
	}, {
		key: 'render',
		value: function render() {
			var Component = this.props.btn.type;
			var _props$btn$props = this.props.btn.props,
			    children = _props$btn$props.children,
			    other = (0, _objectWithoutProperties3.default)(_props$btn$props, ['children']);

			return _react2.default.createElement(
				Component,
				(0, _extends3.default)({ ref: 'btn', onClick: this.handleClick.bind(this) }, other),
				children
			);
		}
	}]);
	return Modal;
}(_react.Component);

//主页


Modal.defaultProps = {
	mid: "modalId", //默认box 的 ID
	//btn: <a>弹窗</a>,//
	modalStyle: {}, //modal的样式
	className: 'animated bounceInDown',
	style: { width: 400, height: 260 }, //框样式
	showBack: null, //显示时候的回调
	closeBack: null //关闭时候的回调
};
var _default = Modal;
exports.default = _default;
;

var _temp = function () {
	if (typeof __REACT_HOT_LOADER__ === 'undefined') {
		return;
	}

	__REACT_HOT_LOADER__.register(Modal, 'Modal', 'dev/mtui/modal/Modal.jsx');

	__REACT_HOT_LOADER__.register(_default, 'default', 'dev/mtui/modal/Modal.jsx');
}();

;
//# sourceMappingURL=Modal.js.map