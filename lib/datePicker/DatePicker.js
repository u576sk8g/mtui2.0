'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

require('./datePicker.scss');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _dateCore = require('./dateCore');

var _DatePickerBox = require('./DatePickerBox');

var _DatePickerBox2 = _interopRequireDefault(_DatePickerBox);

var _offset = require('../utils/offset');

var _select = require('../utils/select');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DatePicker = function (_Component) {
	(0, _inherits3.default)(DatePicker, _Component);

	//构造函数
	function DatePicker(props) {
		(0, _classCallCheck3.default)(this, DatePicker);

		var _this = (0, _possibleConstructorReturn3.default)(this, (DatePicker.__proto__ || (0, _getPrototypeOf2.default)(DatePicker)).call(this, props));

		_this.state = {
			show: props.visible,
			set: {
				left: 0,
				top: 0,
				width: 0,
				height: 0
			},
			setWeek: ['日', '一', '二', '三', '四', '五', '六'],
			nowDate: null,
			date: {
				years: [],
				months: [],
				days: [],
				hours: [],
				minutes: [],
				seconds: []
			},
			clear: false
		};
		_this.div = document.createElement('div');
		_this.div.setAttribute('class', 'mt-div');

		_this.firstWeek = 0; //当月第一天是周几
		_this.monthDay = 0; //当月有多少天
		_this.handlerClickEvent = null;

		return _this;
	}

	//设置初始化的时，分，秒


	//默认参数


	(0, _createClass3.default)(DatePicker, [{
		key: 'setHHMMSS',
		value: function setHHMMSS() {
			var hours = [],
			    minutes = [],
			    seconds = [];
			var setArr = function setArr(arr, max) {
				for (var i = 0; i < max; i++) {
					if (i < 10) {
						arr.push('0' + i);
					} else {
						arr.push(i.toString());
					}
				}
			};
			setArr(hours, 12);
			setArr(minutes, 60);
			setArr(seconds, 60);

			this.setState({
				date: {
					years: [],
					months: [],
					days: [],
					hours: hours,
					minutes: minutes,
					seconds: seconds
				}
			});
		}

		//设置days

	}, {
		key: 'setDays',
		value: function setDays() {
			//一共有6*7 = 42 个格子
			var arr = [];
			var date = this.state.nowDate;

			//加一个月
			var addone = (0, _dateCore.addAndDelOneMonth)(date.year, date.month, 'add');
			if (this.firstWeek == 7) {
				for (var i = 1; i <= 42; i++) {
					if (i <= this.monthDay) {
						arr.push({
							day: i,
							month: date.month,
							year: date.year,
							type: 'now',
							active: i === date.day ? true : false
						});
					} else {
						arr.push({
							day: i - this.monthDay,
							month: addone.month,
							year: addone.year,
							type: 'next',
							active: false
						});
					}
				}
			} else {
				//减一个月
				var delone = (0, _dateCore.addAndDelOneMonth)(date.year, date.month, 'del');
				//获取上个月的日期
				var prevMonth = (0, _dateCore.addAndDelOneMonth)(date.year, date.month, 'del').month;
				var prevMonthDay = (0, _dateCore.getMDay)(date.year, prevMonth);
				for (var _i = 1; _i <= 42; _i++) {
					if (_i <= this.firstWeek) {
						arr.push({
							day: prevMonthDay - this.firstWeek + _i,
							month: delone.month,
							year: delone.year,
							type: 'prev',
							active: false
						});
					} else if (_i > this.firstWeek && _i <= this.monthDay + this.firstWeek) {
						arr.push({
							day: _i - this.firstWeek,
							month: date.month,
							year: date.year,
							type: 'now',
							active: _i - this.firstWeek === date.day ? true : false
						});
					} else {
						arr.push({
							day: _i - this.monthDay - this.firstWeek,
							month: addone.month,
							year: addone.year,
							type: 'next',
							active: false
						});
					}
				}
			}
			return arr;
		}

		//设置month

	}, {
		key: 'setMonths',
		value: function setMonths() {
			var arr = [];
			for (var i = 0; i < 12; i++) {
				arr.push({
					active: i + 1 === this.state.nowDate.month ? true : false,
					year: this.state.nowDate.year,
					month: i + 1
				});
			}
			return arr;
		}

		//重新渲染

	}, {
		key: 'resetDays',
		value: function resetDays(date, callback) {
			var self = this;
			this.setState({
				nowDate: date
			}, function () {
				//当月第一天周几
				self.firstWeek = (0, _dateCore.weekNumber)(date.year, date.month, 1);
				//当月有多少天
				self.monthDay = (0, _dateCore.getMDay)(date.year, date.month);
				//设置days
				var days = self.setDays();
				var months = self.setMonths();
				self.setState({
					date: {
						days: days,
						months: months
					}
				}, function () {
					if (callback) {
						callback();
					}
				});
			});
		}

		//

	}, {
		key: 'componentWillMount',
		value: function componentWillMount() {

			//init 时 分 秒
			this.setHHMMSS();

			//初始化日期 默认是当前日期
			var date = null;
			if (this.props.defaultValue) {
				var val = this.props.defaultValue.split('-');
				date = {
					year: parseInt(val[0], 10),
					month: parseInt(val[1], 10),
					day: parseInt(val[2], 10)
				};
			} else {
				date = (0, _dateCore.getDateNow)();
				this.setState({
					clear: true
				});
			}
			this.resetDays(date);
		}

		//渲染div

	}, {
		key: 'renderDiv',
		value: function renderDiv() {
			var self = this;
			var dom = document.getElementById(this.props.mid);
			//首次渲染即可
			if (!dom) {
				document.body.appendChild(this.div);
				_reactDom2.default.render(_react2.default.createElement(_DatePickerBox2.default, (0, _extends3.default)({ resetDays: this.resetDays.bind(this) }, this.state, { showOrHide: this.showOrHide.bind(this) }, this.props)), this.div); //
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			//document.getElementById(this.props.mid); 
			this.div.remove();
			_reactDom2.default.unmountComponentAtNode(this.div);
		}

		//更新弹窗里面的数据

	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(prevProps) {
			_reactDom2.default.render(_react2.default.createElement(_DatePickerBox2.default, (0, _extends3.default)({ resetDays: this.resetDays.bind(this) }, this.state, { showOrHide: this.showOrHide.bind(this) }, this.props)), this.div);
		}

		//隐藏显示

	}, {
		key: 'showOrHide',
		value: function showOrHide(show, callback, data) {
			var box = document.getElementById(this.props.mid);

			//如果没有，初始化一个
			if (!box) {
				this.renderDiv();
			}

			this.setState({
				show: !show
			}, function () {
				var self = this;
				if (self.state.show) {
					if (self.props.showBack) {
						self.props.showBack();
					}
				} else {
					if (self.props.onChange && data) {
						self.setState({
							clear: data.clear
						});

						//数据过滤
						for (var key in data.obj) {
							if (data.obj[key]) {
								data.obj[key] = parseInt(data.obj[key], 10);
							} else {
								delete data.obj[key];
							}
						}
						data.str = data.str.replace(/-NaN/g, '');
						data.str = data.str.replace(/-undefined/g, '');
						self.props.onChange(data);
					}
				}
				if (callback) {
					callback();
				}

				if (show) {
					document.removeEventListener('click', self.handlerClickEvent);
				}
			});
		}

		//点击空白隐藏弹窗

	}, {
		key: 'clickBlank',
		value: function clickBlank() {
			var self = this;
			//通过判断点击的坐标，判断是否点击在弹框内
			var clickInWindow = function clickInWindow(e) {
				var set = self.state.set;
				//日历宽 230 * 280
				if (e.clientX >= set.left && e.clientX <= set.left + 230) {
					var top = e.clientY + document.body.scrollTop - self.refs.btn.offsetHeight;
					if (top >= set.top && top <= set.top + 280) {
						console.log('top');
						return true;
					}
				}
				return false;
			};

			//点击事件
			this.handlerClickEvent = function (e) {
				if (!clickInWindow(e)) {
					self.showOrHide(true);
				}
			};
			document.removeEventListener('click', self.handlerClickEvent);
			document.addEventListener('click', self.handlerClickEvent);
		}

		//点击事件

	}, {
		key: 'handleClick',
		value: function handleClick(e) {
			var self = this;
			//设置set
			this.setState({
				set: {
					left: (0, _offset.offsetLeft)(self.refs.btn),
					top: (0, _offset.offsetTop)(self.refs.btn),
					width: self.refs.btn.offsetWidth,
					height: self.refs.btn.offsetHeight
				}
			}, function () {
				self.showOrHide(self.state.show);
				//绑定点击空白事件
				self.clickBlank();
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    mid = _props.mid,
			    format = _props.format,
			    showBack = _props.showBack,
			    modalClassName = _props.modalClassName,
			    defaultValue = _props.defaultValue,
			    className = _props.className,
			    size = _props.size,
			    other = (0, _objectWithoutProperties3.default)(_props, ['mid', 'format', 'showBack', 'modalClassName', 'defaultValue', 'className', 'size']);

			var dayval = (0, _dateCore.formatDate)(this.state.nowDate, this.props.format).val;
			var cName = (className ? className + ' ' : '') + 'mt-input-' + (size ? size : 'nm') + ' mt-input mt-input-date';
			return _react2.default.createElement(
				'span',
				{ ref: 'btn', className: cName, onClick: this.handleClick.bind(this) },
				_react2.default.createElement('input', (0, _extends3.default)({
					value: this.state.clear ? '' : dayval,
					readOnly: true,
					type: 'text',
					placeholder: '\u65E5\u671F'
				}, other)),
				_react2.default.createElement(
					'span',
					{ className: 'mt-input-suffix' },
					_react2.default.createElement('i', { className: 'iconfont icon-date' })
				)
			);
		}
	}]);
	return DatePicker;
}(_react.Component);

//主页


DatePicker.defaultProps = {
	mid: "dateId_" + new Date().getTime(), //默认box 的 ID
	placeholder: '日期',
	showBack: null, //显示时候的回调
	onChange: null, //关闭时候的回调
	modalClassName: '',
	defaultValue: '',
	format: 'yyyy-mm-dd' //格式化
};
var _default = DatePicker;
exports.default = _default;
;

var _temp = function () {
	if (typeof __REACT_HOT_LOADER__ === 'undefined') {
		return;
	}

	__REACT_HOT_LOADER__.register(DatePicker, 'DatePicker', 'dev/mtui/datePicker/DatePicker.jsx');

	__REACT_HOT_LOADER__.register(_default, 'default', 'dev/mtui/datePicker/DatePicker.jsx');
}();

;
//# sourceMappingURL=DatePicker.js.map