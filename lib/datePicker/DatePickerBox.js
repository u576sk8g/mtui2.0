'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

require('./datePicker.scss');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _assign = require('../utils/assign');

var _assign2 = _interopRequireDefault(_assign);

var _dateCore = require('./dateCore');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DatePickerBox = function (_Component) {
	(0, _inherits3.default)(DatePickerBox, _Component);

	//构造函数
	function DatePickerBox(props) {
		(0, _classCallCheck3.default)(this, DatePickerBox);

		var _this = (0, _possibleConstructorReturn3.default)(this, (DatePickerBox.__proto__ || (0, _getPrototypeOf2.default)(DatePickerBox)).call(this, props));

		_this.state = {
			show: 'day',
			headtime: null,
			yearList: [],
			defaultShow: 'day'
		};
		return _this;
	}

	//上一页


	(0, _createClass3.default)(DatePickerBox, [{
		key: 'prevClick',
		value: function prevClick() {
			console.log('上一页');
			var _props$nowDate = this.props.nowDate,
			    year = _props$nowDate.year,
			    month = _props$nowDate.month,
			    day = _props$nowDate.day;

			var obj = (0, _dateCore.addAndDelOneMonth)(year, month, 'del');
			obj['day'] = day;
			this.props.resetDays(obj);
			this.setHeadTime(obj);
		}

		//下一页

	}, {
		key: 'nextClick',
		value: function nextClick() {
			console.log('下一页');
			var _props$nowDate2 = this.props.nowDate,
			    year = _props$nowDate2.year,
			    month = _props$nowDate2.month,
			    day = _props$nowDate2.day;

			var obj = (0, _dateCore.addAndDelOneMonth)(year, month, 'add');
			obj['day'] = day;
			this.props.resetDays(obj);
			this.setHeadTime(obj);
		}

		//上一层的上一页

	}, {
		key: 'prevOutClick',
		value: function prevOutClick() {
			var _props$nowDate3 = this.props.nowDate,
			    year = _props$nowDate3.year,
			    month = _props$nowDate3.month,
			    day = _props$nowDate3.day;

			var self = this;
			year = parseInt(year, 10);
			var obj = {
				year: this.state.show === 'year' ? year - 30 : --year,
				month: month,
				day: day
			};
			this.setHeadTime(obj);
			this.props.resetDays(obj, function () {
				if (self.state.show === 'year') {
					self.changeBox();
				}
			});
		}

		//上一层的上一页

	}, {
		key: 'nextOutClick',
		value: function nextOutClick() {
			var _props$nowDate4 = this.props.nowDate,
			    year = _props$nowDate4.year,
			    month = _props$nowDate4.month,
			    day = _props$nowDate4.day;

			var self = this;
			year = parseInt(year, 10);
			var obj = {
				year: this.state.show === 'year' ? year + 30 : ++year,
				month: month,
				day: day
			};
			this.setHeadTime(obj);
			this.props.resetDays(obj, function () {
				if (self.state.show === 'year') {
					self.changeBox();
				}
			});
		}
	}, {
		key: 'iniYear',
		value: function iniYear(year) {
			year = parseInt(year, 10);
			var yearList = [];
			for (var i = year - 10; i < year + 20; i++) {
				yearList.push({
					year: i,
					type: 'year',
					active: i === year ? true : false
				});
			}
			this.setState({
				show: 'year',
				headtime: year - 10 + '-' + (year + 19),
				yearList: yearList
			});
		}

		//切换年，月，日的选择

	}, {
		key: 'changeBox',
		value: function changeBox() {
			if (this.state.show == 'day') {
				this.setState({
					show: 'month',
					headtime: (0, _dateCore.formatDate)(this.props.nowDate, 'yyyy').val
				});
			} else {
				// if(this.state.show == 'month')
				var year = (0, _dateCore.formatDate)(this.props.nowDate, 'yyyy').val;
				this.iniYear(year);
			}
		}

		//设置年

	}, {
		key: 'clickYear',
		value: function clickYear(elem) {

			if (elem.active) {
				return;
			}

			//日这里需要做判断，年份变化，2月可能没有29号
			var maxDay = (0, _dateCore.getMDay)(elem.year, elem.month);
			var _props$nowDate5 = this.props.nowDate,
			    day = _props$nowDate5.day,
			    month = _props$nowDate5.month;

			this.props.resetDays({
				year: elem.year,
				month: month,
				day: day > maxDay ? maxDay : day
			});

			//没有月份的选择了
			if (this.state.defaultShow == 'year') {
				this.iniYear(elem.year);
			} else {
				this.setState({
					show: 'month'
				}, function () {
					this.setHeadTime(elem);
				}.bind(this));
			}
		}

		//选择月

	}, {
		key: 'clickMonth',
		value: function clickMonth(elem) {
			this.setHeadTime(elem);
			if (elem.active) {
				return;
			}
			//日这里需要做判断，年份变化，2月可能没有29号
			var maxDay = (0, _dateCore.getMDay)(elem.year, elem.month);
			var day = this.props.nowDate.day;

			this.props.resetDays({
				year: elem.year,
				month: elem.month,
				day: day > maxDay ? maxDay : day
			});

			//如果没有天的选择
			if (this.state.defaultShow == 'month') {
				this.setHeadTime(elem);
			} else {
				this.setState({
					show: 'day'
				}, function () {
					this.setHeadTime(elem);
				}.bind(this));
			}
		}

		//选择天

	}, {
		key: 'clickDay',
		value: function clickDay(elem) {

			var maxDay = (0, _dateCore.getMDay)(elem.year, elem.month);
			this.props.resetDays({
				year: elem.year,
				month: elem.month,
				day: elem.day > maxDay ? maxDay : elem.day
			});
			//console.log('DatePickerBox 134 >> 当前选择的日期：',elem)
		}

		//设置show,和 headtime

	}, {
		key: 'setHeadTime',
		value: function setHeadTime(obj) {
			if (this.state.show == 'day') {
				this.setState({
					headtime: obj.year + '-' + (0, _dateCore.fliterNum)(obj.month)
				});
			} else if (this.state.show == 'month') {
				this.setState({
					headtime: obj.year
				});
			}
		}

		//判断年，月，日的选项

	}, {
		key: 'dateForFormat',
		value: function dateForFormat() {
			var obj = (0, _dateCore.formatDate)(this.props.nowDate, this.props.format);
			//如果没有天的选择
			if (!obj.show.day) {

				//如果又没有月份
				if (!obj.show.month) {
					this.setState({
						show: 'year',
						defaultShow: 'year'
					});
					this.iniYear(obj.val);
				} else {
					this.setState({
						show: 'month',
						defaultShow: 'month'
					});
				}
			}
		}

		//初始化

	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.setState({
				headtime: (0, _dateCore.formatDate)(this.props.nowDate, 'yyyy-mm').val
			});

			this.dateForFormat();
		}

		//今天

	}, {
		key: 'clickNowDay',
		value: function clickNowDay(str) {
			var obj = (0, _dateCore.getDateNow)();
			this.props.resetDays({
				year: obj.year,
				month: obj.month,
				day: obj.day
			});
		}

		//确定

	}, {
		key: 'clickOk',
		value: function clickOk(str) {
			this.props.showOrHide(true, null, {
				str: (0, _dateCore.formatDate)(this.props.nowDate, 'yyyy-mm-dd').val,
				obj: this.props.nowDate,
				clear: str === 'clear' ? true : false
			});
		}

		//清除

	}, {
		key: 'clearDate',
		value: function clearDate() {
			var self = this;
			this.clickOk('clear');
		}
	}, {
		key: 'render',
		value: function render() {

			//渲染日历
			var _props$date = this.props.date,
			    years = _props$date.years,
			    months = _props$date.months,
			    days = _props$date.days,
			    hours = _props$date.hours,
			    minutes = _props$date.minutes,
			    seconds = _props$date.seconds;

			var self = this;
			//设置定位
			if (this.props.set) {
				var _props$set = this.props.set,
				    height = _props$set.height,
				    left = _props$set.left,
				    top = _props$set.top,
				    width = _props$set.width;


				var style = (0, _assign2.default)([{
					left: left + 230 > document.body.offsetWidth ? document.body.offsetWidth - 230 - 10 : left, //判断left,不能超过body区域
					top: top + height
				}, this.props.modalStyle, { display: this.props.show ? 'block' : 'none' }]);
			}

			return _react2.default.createElement(
				'div',
				{ className: 'mt-date ' + this.props.modalClassName, ref: 'datePicker', id: this.props.mid, style: style },
				_react2.default.createElement(
					'div',
					{ className: 'mt-date-head' },
					_react2.default.createElement(
						'a',
						{ onClick: this.prevOutClick.bind(this), className: 'mt-date-btn-prev' },
						_react2.default.createElement('i', { className: 'iconfont icon-arrow3l' })
					),
					this.state.show === 'day' ? _react2.default.createElement(
						'a',
						{ onClick: this.prevClick.bind(this), className: 'mt-date-btn-prev' },
						_react2.default.createElement('i', { className: 'iconfont icon-arrowl' })
					) : null,
					_react2.default.createElement(
						'span',
						{ onClick: this.changeBox.bind(this), className: 'mt-date-stime' },
						this.state.headtime
					),
					this.state.show === 'day' ? _react2.default.createElement(
						'a',
						{ onClick: this.nextClick.bind(this), className: 'mt-date-btn-next' },
						_react2.default.createElement('i', { className: 'iconfont icon-arrowr' })
					) : null,
					_react2.default.createElement(
						'a',
						{ onClick: this.nextOutClick.bind(this), className: 'mt-date-btn-next' },
						_react2.default.createElement('i', { className: 'iconfont icon-arrow3r' })
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'mt-date-body-years', style: { display: this.state.show === 'year' ? 'block' : 'none' } },
					_react2.default.createElement(
						'div',
						{ className: 'mt-date-year' },
						_react2.default.createElement(
							'ul',
							{ className: 'clearfix' },
							this.state.yearList.map(function (elem, index) {
								return _react2.default.createElement(
									'li',
									{ onClick: self.clickYear.bind(self, elem), key: index, className: "mt-date-yearli" + (elem.active ? ' mt-date-active' : '') },
									_react2.default.createElement(
										'a',
										null,
										elem.year
									)
								);
							})
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'mt-date-body-months', style: { display: this.state.show === 'month' ? 'block' : 'none' } },
					_react2.default.createElement(
						'div',
						{ className: 'mt-date-month' },
						_react2.default.createElement(
							'ul',
							{ className: 'clearfix' },
							this.props.date.months.map(function (elem, index) {
								return _react2.default.createElement(
									'li',
									{ onClick: self.clickMonth.bind(self, elem), key: index, className: "mt-date-month-item" + (elem.active ? ' mt-date-active' : '') },
									_react2.default.createElement(
										'a',
										null,
										elem.month,
										'\u6708'
									)
								);
							})
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'mt-date-body-days', style: { display: this.state.show === 'day' ? 'block' : 'none' } },
					_react2.default.createElement(
						'div',
						{ className: 'mt-date-week' },
						_react2.default.createElement(
							'ul',
							null,
							this.props.setWeek.map(function (elem, index) {
								return _react2.default.createElement(
									'li',
									{ key: index },
									elem
								);
							})
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'mt-date-day' },
						_react2.default.createElement(
							'ul',
							{ className: 'clearfix' },
							this.props.date.days.map(function (elem, index) {
								return _react2.default.createElement(
									'li',
									{ onClick: self.clickDay.bind(self, elem), key: index, className: "mt-date-day-" + elem.type + (elem.active ? ' mt-date-active' : '') },
									_react2.default.createElement(
										'a',
										null,
										elem.day
									)
								);
							})
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'mt-date-foot' },
					_react2.default.createElement(
						'a',
						{ onClick: this.clickNowDay.bind(this), className: 'mt-date-btn-now' },
						'\u4ECA\u5929'
					),
					_react2.default.createElement(
						'a',
						{ className: 'mt-date-btn-time' },
						'\u65F6\u949F'
					),
					_react2.default.createElement(
						'a',
						{ onClick: this.clickOk.bind(this), className: 'mt-btn-xs mt-btn-primary mt-date-btn-ok' },
						'\u786E\u5B9A'
					),
					_react2.default.createElement(
						'a',
						{ onClick: this.clearDate.bind(this), className: 'mt-btn-xs mt-btn-warning mt-date-btn-clear' },
						'\u6E05\u9664'
					)
				)
			);
		}
	}]);
	return DatePickerBox;
}(_react.Component);

//主页


var _default = DatePickerBox;
exports.default = _default;
;

var _temp = function () {
	if (typeof __REACT_HOT_LOADER__ === 'undefined') {
		return;
	}

	__REACT_HOT_LOADER__.register(DatePickerBox, 'DatePickerBox', 'dev/mtui/datePicker/DatePickerBox.jsx');

	__REACT_HOT_LOADER__.register(_default, 'default', 'dev/mtui/datePicker/DatePickerBox.jsx');
}();

;
//# sourceMappingURL=DatePickerBox.js.map