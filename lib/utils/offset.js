"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
* @type 获取dom 行对body的left
* @author : Mantou
*/
function offsetLeft(self) {
    var val = 0;
    while (self) {
        val += self.offsetLeft;
        self = self.offsetParent;
    }
    return val;
}

function offsetTop(self) {
    var val = 0;
    var i = 0;
    while (self) {
        val += self.offsetTop;
        self = self.offsetParent;
    }
    return val;
}

exports.offsetTop = offsetTop;
exports.offsetLeft = offsetLeft;
;

var _temp = function () {
    if (typeof __REACT_HOT_LOADER__ === 'undefined') {
        return;
    }

    __REACT_HOT_LOADER__.register(offsetLeft, "offsetLeft", "dev/mtui/utils/offset.jsx");

    __REACT_HOT_LOADER__.register(offsetTop, "offsetTop", "dev/mtui/utils/offset.jsx");
}();

;
//# sourceMappingURL=offset.js.map