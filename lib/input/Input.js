'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

require('./input.scss');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _offset = require('../utils/offset');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Input = function (_Component) {
	(0, _inherits3.default)(Input, _Component);

	//构造函数
	function Input(props) {
		(0, _classCallCheck3.default)(this, Input);
		return (0, _possibleConstructorReturn3.default)(this, (Input.__proto__ || (0, _getPrototypeOf2.default)(Input)).call(this, props));
	}

	(0, _createClass3.default)(Input, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    size = _props.size,
			    prefix = _props.prefix,
			    suffix = _props.suffix,
			    onPressEnter = _props.onPressEnter,
			    className = _props.className,
			    validateInfo = _props.validateInfo,
			    other = (0, _objectWithoutProperties3.default)(_props, ['size', 'prefix', 'suffix', 'onPressEnter', 'className', 'validateInfo']);


			var cName = (className ? className + ' ' : '') + 'mt-input-' + (!size ? 'nm' : size) + ' mt-input';
			return _react2.default.createElement(
				'span',
				{ className: cName },
				prefix ? _react2.default.createElement(
					'span',
					{ className: 'mt-input-prefix' },
					prefix
				) : null,
				_react2.default.createElement('input', other),
				suffix ? _react2.default.createElement(
					'span',
					{ className: 'mt-input-suffix' },
					suffix
				) : null,
				validateInfo
			);
		}
	}]);
	return Input;
}(_react.Component);

//主页


Input.defaultProps = {
	size: 'nm', //lg , nm, sm, xs 大 ，正常，小，超小
	type: 'text',
	prefix: null, //前面的图标
	suffix: null, //后面的图标
	onPressEnter: null, //回车的回调
	validateInfo: null //表单验证提示
};
var _default = Input;
exports.default = _default;
;

var _temp = function () {
	if (typeof __REACT_HOT_LOADER__ === 'undefined') {
		return;
	}

	__REACT_HOT_LOADER__.register(Input, 'Input', 'dev/mtui/input/Input.jsx');

	__REACT_HOT_LOADER__.register(_default, 'default', 'dev/mtui/input/Input.jsx');
}();

;
//# sourceMappingURL=Input.js.map