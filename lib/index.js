'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.Collapse = exports.Swiper = exports.Validate = exports.Tree = exports.SliderBar = exports.Slider = exports.Panle = exports.Checkbox = exports.Radio = exports.Switch = exports.DatePicker = exports.Tabs = exports.BackTop = exports.Select = exports.Dropdown = exports.Modal = exports.PageList = exports.Tip = exports.Input = exports.Button = exports.Grid = undefined;

require('./public.scss');

var _Grid = require('./grid/Grid');

var _Grid2 = _interopRequireDefault(_Grid);

var _Button = require('./button/Button');

var _Button2 = _interopRequireDefault(_Button);

var _Input = require('./input/Input');

var _Input2 = _interopRequireDefault(_Input);

var _PageList = require('./pagelist/PageList');

var _PageList2 = _interopRequireDefault(_PageList);

var _Modal = require('./modal/Modal');

var _Modal2 = _interopRequireDefault(_Modal);

var _Dropdown = require('./dropDown/Dropdown');

var _Dropdown2 = _interopRequireDefault(_Dropdown);

var _Select = require('./select/Select');

var _Select2 = _interopRequireDefault(_Select);

var _BackTop = require('./backtop/BackTop');

var _BackTop2 = _interopRequireDefault(_BackTop);

var _Tabs = require('./Tabs/Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _DatePicker = require('./DatePicker/DatePicker');

var _DatePicker2 = _interopRequireDefault(_DatePicker);

var _Switch = require('./switch/Switch');

var _Switch2 = _interopRequireDefault(_Switch);

var _Radio = require('./radio/Radio');

var _Radio2 = _interopRequireDefault(_Radio);

var _Checkbox = require('./checkbox/Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _Panle = require('./panle/Panle');

var _Panle2 = _interopRequireDefault(_Panle);

var _Slider = require('./slider/Slider');

var _Slider2 = _interopRequireDefault(_Slider);

var _SliderBar = require('./sliderBar/SliderBar');

var _SliderBar2 = _interopRequireDefault(_SliderBar);

var _Tree = require('./tree/Tree');

var _Tree2 = _interopRequireDefault(_Tree);

var _Validate = require('./validate/Validate');

var _Validate2 = _interopRequireDefault(_Validate);

var _Swiper = require('./swiper/Swiper');

var _Swiper2 = _interopRequireDefault(_Swiper);

var _Collapse = require('./collapse/Collapse');

var _Collapse2 = _interopRequireDefault(_Collapse);

var _Tip = require('./Tip/Tip');

var _Tip2 = _interopRequireDefault(_Tip);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//配置信息
/**
* @type MTUI2.0
* @author : Mantou
*/
exports.Grid = _Grid2.default;
exports.Button = _Button2.default;
exports.Input = _Input2.default;
exports.Tip = _Tip2.default;
exports.PageList = _PageList2.default;
exports.Modal = _Modal2.default;
exports.Dropdown = _Dropdown2.default;
exports.Select = _Select2.default;
exports.BackTop = _BackTop2.default;
exports.Tabs = _Tabs2.default;
exports.DatePicker = _DatePicker2.default;
exports.Switch = _Switch2.default;
exports.Radio = _Radio2.default;
exports.Checkbox = _Checkbox2.default;
exports.Panle = _Panle2.default;
exports.Slider = _Slider2.default;
exports.SliderBar = _SliderBar2.default;
exports.Tree = _Tree2.default;
exports.Validate = _Validate2.default;
exports.Swiper = _Swiper2.default;
exports.Collapse = _Collapse2.default;

//工具

window.MT_IE9 = false; //如果是IE9 就是true
if (navigator.appName == "Microsoft Internet Explorer") {
	window.MT_MS = 'IE';
	var ms = navigator.appVersion.split(";")[1].replace(/[ ]/g, "");
	if (~~ms.replace('MSIE', '') > 9) {
		window.MT_IE9 = false;
	} else {
		window.MT_IE9 = true;
	}
} else {
	window.MT_MS = 'other';
}
;

var _temp = function () {
	if (typeof __REACT_HOT_LOADER__ === 'undefined') {
		return;
	}
}();

;
//# sourceMappingURL=index.js.map