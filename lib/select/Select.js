'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

require('./select.scss');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _SelectModal = require('./SelectModal');

var _SelectModal2 = _interopRequireDefault(_SelectModal);

var _select = require('../utils/select');

var _offset = require('../utils/offset');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Select = function (_Component) {
	(0, _inherits3.default)(Select, _Component);

	//构造函数
	function Select(props) {
		(0, _classCallCheck3.default)(this, Select);

		var _this2 = (0, _possibleConstructorReturn3.default)(this, (Select.__proto__ || (0, _getPrototypeOf2.default)(Select)).call(this, props));

		_this2.state = {
			show: props.visible,
			set: {
				left: null,
				top: null,
				width: null,
				height: null
			},
			text: '',
			value: ''
		};
		_this2.div = document.createElement('div');
		_this2.div.setAttribute('class', 'mt-div');
		return _this2;
	}

	//渲染div


	//默认参数


	(0, _createClass3.default)(Select, [{
		key: 'renderDiv',
		value: function renderDiv() {
			var _this = this;
			var dom = document.getElementById(this.props.mid);
			//首次渲染即可
			if (!dom) {
				this.setState({
					set: {
						left: (0, _offset.offsetLeft)(this.refs.btn),
						top: (0, _offset.offsetTop)(this.refs.btn),
						width: this.refs.btn.offsetWidth,
						height: this.refs.btn.offsetHeight
					}
				});
				document.body.appendChild(this.div);
				this.renderDOM();
			}
		}
	}, {
		key: 'renderDOM',
		value: function renderDOM() {
			_reactDom2.default.render(_react2.default.createElement(_SelectModal2.default, (0, _extends3.default)({}, this.state, { showOrHide: this.showOrHide.bind(this), show: this.state.show, set: this.state.set }, this.props)), this.div);
		}

		//更新弹窗里面的数据

	}, {
		key: 'componentDidUpdate',
		value: function componentDidUpdate(prevProps) {

			//受控组件
			if (this.props.value || this.props.defaultValue) {
				//console.log('受控组件',this.props.value,prevProps.value);
				this.renderDOM();
			}
		}

		//受控组件方法

	}, {
		key: 'componentWillUpdate',
		value: function componentWillUpdate(nextProps, nextState) {
			if (this.props.value && this.props.value != nextProps.value) {
				var self = this;
				nextProps.children.map(function (elem, index) {
					if (elem.props.value == nextProps.value) {
						self.setValue(elem.props);
					}
				});
			}
		}

		//设置value

	}, {
		key: 'setValue',
		value: function setValue(data, callback) {
			this.setState({
				text: data.children,
				value: data.value
			}, function () {
				if (callback) {
					callback(data);
				}
			});
		}

		//隐藏显示

	}, {
		key: 'showOrHide',
		value: function showOrHide(show, data, callback, value) {

			//console.log(show,data)
			this.hoverHandler = null;
			var self = this;
			this.renderDiv();
			this.setState({
				show: !show,
				set: {
					left: (0, _offset.offsetLeft)(this.refs.btn),
					top: (0, _offset.offsetTop)(this.refs.btn),
					width: this.refs.btn.offsetWidth,
					height: this.refs.btn.offsetHeight
				}
			}, function () {
				if (!show) {
					//显示弹窗
					if (self.props.showBack) {
						self.props.showBack();
					}
				} else {
					//选择值
					if (self.props.onChange && data) {
						if (this.props.value) {
							self.props.onChange(data);
							return;
						} else {
							this.setValue(data, self.props.onChange(data));
						}
					}
				}
				if (callback) {
					callback();
				}
			});
		}

		//点击其他区域，隐藏下拉框

	}, {
		key: 'triggerBlank',
		value: function triggerBlank(events) {
			var _this = this;

			var callback = function callback() {
				document.removeEventListener(events, handler);
			};
			var handler = function handler(e) {
				var self = this;
				if (!(0, _select.closest)(e.target, '.mt-select') && !(0, _select.closest)(e.target, '.mt-select-input')) {
					_this.showOrHide(true, null, callback);
				}
			};
			document.addEventListener(events, handler);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			this.refs.btn.removeEventListener('mouseenter', this.hoverHandler);
		}

		//初始化状态

	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {

			var self = this;
			//如果默认是显示，直接显示DIV
			if (this.props.visible) {
				self.renderDiv();
			}

			//设置默认值
			if (this.props.defaultValue || this.props.value) {
				this.props.children.map(function (elem, index) {
					if (elem.props.value == self.props.defaultValue || elem.props.value == self.props.value) {
						self.setState({
							text: elem.props.children,
							value: elem.props.value
						});
					}
				});
			}

			//hover事件
			if (this.props.trigger == 'hover') {
				var _this = this;

				this.hoverHandler = function (e) {
					_this.showOrHide(false, null, function () {
						_this.triggerBlank('mousemove');
					});
				};
				this.refs.btn.addEventListener('mouseenter', this.hoverHandler);
			}
		}

		//点击事件

	}, {
		key: 'handleClick',
		value: function handleClick() {
			var _this = this;
			if (this.props.trigger == 'click') {
				this.showOrHide(this.state.show, null, function () {
					_this.triggerBlank('click');
				});
			} else {
				return;
			}
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ style: { width: this.props.modalStyle.width || 120 }, ref: 'btn', className: 'mt-select-input', onClick: this.handleClick.bind(this) },
				_react2.default.createElement(
					'span',
					null,
					this.state.text || _react2.default.createElement(
						'i',
						{ className: 'mt-placeholder' },
						this.props.placeholder
					)
				),
				_react2.default.createElement('i', { className: 'iconfont icon-arrowd' })
			);
		}
	}]);
	return Select;
}(_react.Component);

//选项卡


Select.defaultProps = {
	mid: "selectId", //默认box 的 ID
	modalStyle: { width: 90 }, //modal的样式
	visible: false, //框默认隐藏
	showBack: null, //显示时候的回调
	onChange: null, //关闭时候的回调
	trigger: "hover", //触发下拉的行为
	placeholder: '下拉选择框',
	defaultValue: undefined, //不受控组件
	value: undefined //受控组件
};

var Option = function (_Component2) {
	(0, _inherits3.default)(Option, _Component2);

	function Option() {
		(0, _classCallCheck3.default)(this, Option);
		return (0, _possibleConstructorReturn3.default)(this, (Option.__proto__ || (0, _getPrototypeOf2.default)(Option)).apply(this, arguments));
	}

	(0, _createClass3.default)(Option, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'span',
				null,
				this.props.children
			);
		}
	}]);
	return Option;
}(_react.Component);

Select.Option = Option;

//主页
var _default = Select;
exports.default = _default;
;

var _temp = function () {
	if (typeof __REACT_HOT_LOADER__ === 'undefined') {
		return;
	}

	__REACT_HOT_LOADER__.register(Select, 'Select', 'dev/mtui/select/Select.jsx');

	__REACT_HOT_LOADER__.register(Option, 'Option', 'dev/mtui/select/Select.jsx');

	__REACT_HOT_LOADER__.register(_default, 'default', 'dev/mtui/select/Select.jsx');
}();

;
//# sourceMappingURL=Select.js.map